package business;

import beans.Order;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Kelly Lamb
 */
@RequestScoped
@Path("/orders")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class OrdersRestService {

    @Context
    private UriInfo context;

    @Inject
    OrdersBusinessInterface service;

    /**
     * Creates a new instance of OrdersRestService
     */
    public OrdersRestService() {
    }

    
    @GET
    @Path("/getjson")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Order> getOrdersAsJson() {
        
        return service.getOrders();
    }
    
    @GET
    @Path("/getxml")
    @Produces(MediaType.APPLICATION_XML)
    public Order[] getOrdersAsXml() {
        
        Order[] orders = new Order[service.getOrders().size()];
        orders = service.getOrders().toArray(orders);
        return orders;
    }

    /**
     * PUT method for updating or creating an instance of OrdersRestService
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
