package business;


/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-03-21
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 6a - Test EJBs
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create EJBs
 * 2. Create interfaces and concrete classes to inject/configure
 * 3. Timer Service beans/services
 * ---------------------------------------------------------------
 */


// This should be a Message-Driven Bean but not sure how to get the 
// IDE configured to allow me to create it.
// If I could just find a complete sample code, I might be able to
// write around the IDE but ...!
public class OrderMessageService {
    
    // onMessage()
    
    // onSendOrder()
    
    
    
    
}
