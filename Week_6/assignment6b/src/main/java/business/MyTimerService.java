
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-03-01
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 6a - Test EJBs
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create EJBs
 * 2. Create interfaces and concrete classes to inject/configure
 * 3. Timer Service beans/services
 * ---------------------------------------------------------------
 */

package business;

import javax.annotation.Resource;
import java.util.Date;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.ejb.Timeout;

@Stateless
@LocalBean
public class MyTimerService {

    @Resource
    TimerService timerService;

    private static final Logger logger = Logger.getLogger("business.MyTimerService");

    public MyTimerService() {}

    public void myTimer() {
        System.out.println("Timer event: " + new Date());
    }

    public void setTimer(long interval) {
        timerService.createTimer(interval, "MyTimer");
    }

    @Timeout
    public void programmicTimer(Timer timer) {
        logger.info("@Timeout in programmicTimer at " + new Date());
    }

    @Schedule(dayOfWeek = "*", month = "*", hour = "0-23", dayOfMonth = "*", year = "*", minute = "*", second = "*/10", info="MyTimer")
    private void scheduleTimeout(final Timer t) {
        logger.info("@scheduleTimeout triggered at " + new Date());
    }

}
