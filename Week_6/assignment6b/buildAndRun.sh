#!/bin/sh
mvn clean package && docker build -t klamb/assignment6b .
docker rm -f assignment6b || true && docker run -d -p 9080:9080 -p 9443:9443 --name assignment6b klamb/assignment6b