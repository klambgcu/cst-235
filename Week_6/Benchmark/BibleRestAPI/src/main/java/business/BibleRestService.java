package business;

import beans.BibleDTO;
import beans.BibleVerse;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2021-03-21
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - Benchmark Bible Rest API
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Bible Rest API 
 * 2. API References options
 *    1. Given a word, the service will return the exact first occurrence of the word (if it exists). 
 *       The result will include the book name, chapter number, and verse number
 *    2. Given a word, the service will return the number of occurrences of that word
 *    3. Given a book name, chapter number, and verse number, the service will return the full verse
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * REST Web Service
 *
 * @author Kelly Lamb
 */
@RequestScoped
@Path("/bible")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class BibleRestService {

    @Context
    private UriInfo context;

    @Inject
    BibleBusinessInterface service;

    
    /**
     * Creates a new instance of BibleRestService
     */
    public BibleRestService() {
    }

    /**
     * Retrieves representation of an instance of business.BibleRestService
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/firstverse/{searchword}")
    @Produces(MediaType.APPLICATION_JSON)
    public BibleDTO getFirstVerse(@PathParam("searchword") String searchword) {
        
        BibleDTO bibleDTO = new BibleDTO();
        BibleVerse bibleVerse = service.findFirstExactWordMatchInVerse(searchword);
        
        if (bibleVerse == null) {
            
            // Something went wrong - 500
            bibleDTO.setCode(500);
            bibleDTO.setMessage("Error: Something went wrong - try again later or contact administrator.");
            
        } else if (bibleVerse.getChapterNo() == 0) {

            // No record found - 404
            bibleDTO.setCode(404);
            bibleDTO.setMessage("No record found.");
            
        } else {
            
            // Record found, status okay - 200
            bibleDTO.setCode(200);
            bibleDTO.setMessage("Record found.");
            bibleDTO.getVerses().add(bibleVerse);
        }

        return bibleDTO;
    }

}
