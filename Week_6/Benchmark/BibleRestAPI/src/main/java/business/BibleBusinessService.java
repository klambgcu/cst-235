package business;

import beans.BibleBookName;
import beans.BibleSectionName;
import beans.BibleVerse;
import database.DatabaseService;
import java.util.List;
import java.util.ArrayList;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2021-03-21
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - Benchmark Bible Rest API
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Bible Rest API 
 * 2. API References options
 *    1. Given a word, the service will return the exact first occurrence of the word (if it exists). 
 *       The result will include the book name, chapter number, and verse number
 *    2. Given a word, the service will return the number of occurrences of that word
 *    3. Given a book name, chapter number, and verse number, the service will return the full verse
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * BibleBusinessService implements business requirements for the Rest API functionality
 * @author Kelly Lamb
 */
@Stateless
@Local(BibleBusinessInterface.class)
@Alternative
public class BibleBusinessService implements BibleBusinessInterface {

    private final DatabaseService db = new DatabaseService();
    
    @Override
    public List<BibleVerse> readAllBibleVerses() {
        return db.readAllBibleVerses();
    }

    @Override
    public BibleVerse findFirstExactWordMatchInVerse(String searchWord) {
        return db.findFirstExactWordMatchInVerse(searchWord);
    }

    @Override
    public BibleBookName getBibleBookNameByID(int bookID) {
        return db.getBibleBookNameByID(bookID);
    }

    @Override
    public BibleSectionName getBibleSectionNameByID(int sectionID) {
        return db.getBibleSectionNameByID(sectionID);
    }
    
}
