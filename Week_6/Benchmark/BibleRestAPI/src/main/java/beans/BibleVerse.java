package beans;

import java.io.Serializable;
import javax.faces.bean.ViewScoped;
import javax.faces.bean.ManagedBean;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2021-03-21
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - Benchmark Bible Rest API
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Bible Rest API 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */


/**
 * BibleVerse - An object to hold Bible Verse Text and position in
 * Book, Chapter, and Verse number
 * @author Kelly Lamb
 */

@ViewScoped
@ManagedBean
@XmlRootElement(name="BibleVerse") 
public class BibleVerse implements Serializable {
    
    private String BookName;
    private int ChapterNo;    
    private int VerseNo;
    private String VerseText;
    
    public BibleVerse() {
        this.BookName = "";
        this.ChapterNo = 0;
        this.VerseNo = 0;
        this.VerseText = "";
    }

    public BibleVerse(String BookName, int ChapterNo, int VerseNo, String VerseText) {
        this.BookName = BookName;
        this.ChapterNo = ChapterNo;
        this.VerseNo = VerseNo;
        this.VerseText = VerseText;
    }

    public String getBookName() {
        return BookName;
    }

    public void setBookName(String BookName) {
        this.BookName = BookName;
    }

    public int getChapterNo() {
        return ChapterNo;
    }

    public void setChapterNo(int ChapterNo) {
        this.ChapterNo = ChapterNo;
    }

    public int getVerseNo() {
        return VerseNo;
    }

    public void setVerseNo(int VerseNo) {
        this.VerseNo = VerseNo;
    }

    public String getVerseText() {
        return VerseText;
    }

    public void setVerseText(String VerseText) {
        this.VerseText = VerseText;
    }
    
    @Override
    public String toString() {
        return "BookName=" + this.BookName +
               " ChapterNo=" + this.ChapterNo +
               " VerseNo=" + this.VerseNo +
               " VerseText=" + this.VerseText;
    }
}
