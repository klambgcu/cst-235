package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2021-03-21
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - Benchmark Bible Rest API
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Bible Rest API
 * 2. API References options
 *    1. Given a word, the service will return the exact first occurrence of the word (if it exists).
 *       The result will include the book name, chapter number, and verse number
 *    2. Given a word, the service will return the number of occurrences of that word
 *    3. Given a book name, chapter number, and verse number, the service will return the full verse
 * 3.
 * ---------------------------------------------------------------
 */


/**
 *
 * @author Kelly Lamb
 */
@ViewScoped
@ManagedBean
@XmlRootElement(name="BibleVerse")
public class BibleDTO implements Serializable {

	private List<BibleVerse> verses = null;
	private int code = 0;
	private String message = "";

	public BibleDTO()
	{
		verses = new ArrayList<>();
		code = 0;
		message = "";
	}

	public BibleDTO(List<BibleVerse> verses, int code, String message)
	{
		this.verses = verses;
		this.code = code;
		this.message = message;
	}

	public List<BibleVerse> getVerses() {

		return this.verses;
	}

	public void setVerses(List<BibleVerse> verses) {

		this.verses = verses;
	}

	public int getCode() {

		return this.code;
	}

	public void setCode(int code) {

		this.code = code;
	}

	public String getMessage() {

		return this.message;
	}

	public void setMessage(String message) {

		this.message = message;
	}
}
