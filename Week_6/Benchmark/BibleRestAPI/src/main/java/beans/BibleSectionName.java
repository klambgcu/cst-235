package beans;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2021-03-21
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - Benchmark Bible Rest API
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Bible Rest API 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * BibleSectionName holds the name of the section a bible book resides.
 * Ex. Old Testament, Major/Minor Prophets, Epistles, etc.
 * @author Kelly Lamb
 */
public class BibleSectionName {
    private int SectionID;
    private String SectionName;

    public BibleSectionName() {
        this.SectionID = 1;
        this.SectionName = "";
    }

    public BibleSectionName(int SectionID, String SectionName) {
        this.SectionID = SectionID;
        this.SectionName = SectionName;
    }

    public int getSectionID() {
        return SectionID;
    }

    public void setSectionID(int SectionID) {
        this.SectionID = SectionID;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String SectionName) {
        this.SectionName = SectionName;
    }
    

    
}
