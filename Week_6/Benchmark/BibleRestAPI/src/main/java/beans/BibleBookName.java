package beans;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2021-03-21
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - Benchmark Bible Rest API
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Bible Rest API 
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * BibleBookName holds the name of the book and its ordered ID within
 * the overall KJV Bible.
 * @author Kelly Lamb
 */
public class BibleBookName {
    private int BookID;
    private String BookName;
    
    public BibleBookName () {
        this.BookID = 1;
        this.BookName = "";
    }

    public BibleBookName(int BookID, String BookName) {
        this.BookID = BookID;
        this.BookName = BookName;
    }

    public int getBookID() {
        return BookID;
    }

    public void setBookID(int BookID) {
        this.BookID = BookID;
    }

    public String getBookName() {
        return BookName;
    }

    public void setBookName(String BookName) {
        this.BookName = BookName;
    }
    
    
}
