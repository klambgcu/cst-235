package database;

import beans.BibleBookName;
import beans.BibleSectionName;
import beans.BibleVerse;
import java.util.List;
import javax.ejb.Local;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2021-03-21
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - Benchmark Bible Rest API
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Bible Rest API 
 * 2. API References options
 *    1. Given a word, the service will return the exact first occurrence of the word (if it exists). 
 *       The result will include the book name, chapter number, and verse number
 *    2. Given a word, the service will return the number of occurrences of that word
 *    3. Given a book name, chapter number, and verse number, the service will return the full verse
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * Definition for the Database Interface
 * @author Kelly Lamb
 */
@Local
public interface DatabaseInterface {
    
    //
    // All exposed methods for the Bible DAO
    //
    public List<BibleVerse> readAllBibleVerses();
    public BibleVerse findFirstExactWordMatchInVerse(String searchWord);
    public BibleBookName getBibleBookNameByID(int bookID);
    public BibleSectionName getBibleSectionNameByID(int sectionID);
}
