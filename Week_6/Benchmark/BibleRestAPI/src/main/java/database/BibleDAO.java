package database;

import beans.BibleBookName;
import beans.BibleSectionName;
import beans.BibleVerse;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2021-03-21
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Milestone - Benchmark Bible Rest API
 * Disclaimer: This is our own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Bible Rest API 
 * 2. API References options
 *    1. Given a word, the service will return the exact first occurrence of the word (if it exists). 
 *       The result will include the book name, chapter number, and verse number
 *    2. Given a word, the service will return the number of occurrences of that word
 *    3. Given a book name, chapter number, and verse number, the service will return the full verse
 * 3. 
 * ---------------------------------------------------------------
 */

/**
 * BibleDAO encapsulates all database queries
 * @author Kelly Lamb
 */
public class BibleDAO {
 
    private final static String CONNECT_CLASS = "com.mysql.cj.jdbc.Driver";
    private final static String DBURL = "jdbc:mysql://localhost:3306/bibledb";
    private final static String DBBLOG = "bibleuser";
    private final static String DBPASS = "biblepassword";

    private final static String SQL_SELECT_ALL_BIBLEVERSE = "SELECT bk.*, b.bookname FROM bibledb_kjv bk, bibledb_bookindex b AND bk.bookid = b.bookid ORDER BY bk.bookid, bk.chapterno, bk.verseno";
    private final static String SQL_FIND_EXACT_WORD = "SELECT bk.*, b.bookname FROM bibledb_kjv bk, bibledb_bookindex b WHERE bk.versetext REGEXP ? AND bk.bookid = b.bookid ORDER BY bk.bookid, bk.chapterno, bk.verseno LIMIT 1";
//    private final static String SQL_FIND_EXACT_WORD = "SELECT * FROM bibledb_kjv WHERE versetext REGEXP '[[:<:]]?[[:>:]]' ORDER BY bookid, chapterno, verseno LIMIT 1";
    private final static String SQL_GET_BIBLE_BOOK_BY_ID = "SELECT * FROM bibledb_bookindex WHERE bookid = ?";
    private final static String SQL_GET_BIBLE_SECTION_BY_ID = "SELECT * FROM bibledb_sectionindex WHERE sectionid = ?";
    
    /**
     * 
     * Default Constructor - Contains CRUD database methods for blogs table/objects
     */  
    public BibleDAO() { 
    }

     /**
     * 
     * @return List of all Bible Verses for KJV in the database
     */
    public List<BibleVerse> readAllBibleVerses() {
        List<BibleVerse> bibleVerses = new ArrayList<>();

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_BIBLEVERSE);) {

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String bookname = rs.getString("bookname");
                int chapterno = rs.getInt("chapterno");
                int verseno = rs.getInt("verseno");
                String verseText = rs.getString("versetext");
                bibleVerses.add(new BibleVerse(bookname, chapterno, verseno, verseText));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return bibleVerses;
    }

    /**
    * 
    * @return The first bible verse containing the exact word 
    * match ordered ascending Book Number, Chapter Number, Verse Number
    */
    public BibleVerse findFirstExactWordMatchInVerse(String searchWord) {
        BibleVerse bibleVerse = new BibleVerse(); // No record found - 404
        
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_EXACT_WORD);) {
            
            //  '[[:<:]]?[[:>:]]' - Word Boundary (Exact Match)
            // trim() - remove leading/trailing blanks
            preparedStatement.setString(1, "[[:<:]]" + searchWord.trim() + "[[:>:]]"  ); 

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String bookname = rs.getString("bookname");
                int chapterno = rs.getInt("chapterno");
                int verseno = rs.getInt("verseno");
                String verseText = rs.getString("versetext");
                bibleVerse = new BibleVerse(bookname, chapterno, verseno, verseText);
                // everything good - 200
            }
        } catch (SQLException e) {
            bibleVerse = null; // somethingwent wrong - 500
            printSQLException(e);
        } catch (Exception e) {
            bibleVerse = null; // something went wrong - 500
            e.printStackTrace();
        }

        return bibleVerse;
    }

    
    /**
    * 
    * @return Bible Book Name By ID
    */
    public BibleBookName getBibleBookNameByID(int bookID) {
        BibleBookName bookName = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_BIBLE_BOOK_BY_ID);) {

            preparedStatement.setInt(1, bookID);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int bookid = rs.getInt("bookid");
                String bookname = rs.getString("bookname");
                bookName = new BibleBookName(bookid, bookname);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return bookName;
    }

    /**
    * 
    * @return Section Name By ID (Ex. Old Testament, New Testament, etc.)
    */
    public BibleSectionName getBibleSectionNameByID(int sectionID) {
        BibleSectionName sectionName = null;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_BIBLE_BOOK_BY_ID);) {

            preparedStatement.setInt(1, sectionID);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                int sectionid = rs.getInt("sectionid");
                String sectionname = rs.getString("sectionname");
                sectionName = new BibleSectionName(sectionid, sectionname);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return sectionName;
    }


    // --------------------------------------------------------
    // Helper methods below
    // --------------------------------------------------------
    
    /**
     *
     * @return connection : a helper method to return a connection to the database
     */
    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(CONNECT_CLASS);
            connection = DriverManager.getConnection(DBURL, DBBLOG, DBPASS);
        } catch (SQLException e) {
            printSQLException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * 
     * @param exceptions : a helper method to log exception error notifications
     */
    protected void printSQLException(SQLException exceptions) {
        for (Throwable e : exceptions) {
            if (e instanceof SQLException) {
                e.printStackTrace();
                System.out.println("SQLState: " + ((SQLException) e).getSQLState());
                System.out.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.out.println("Message: " + e.getMessage());
                Throwable t = exceptions.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }


}
