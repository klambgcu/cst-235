@echo off
call mvn clean package
call docker build -t bibledb/BibleRestAPI .
call docker rm -f BibleRestAPI
call docker run -d -p 9080:9080 -p 9443:9443 --name BibleRestAPI bibledb/BibleRestAPI