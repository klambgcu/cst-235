#!/bin/sh
mvn clean package && docker build -t bibledb/BibleRestAPI .
docker rm -f BibleRestAPI || true && docker run -d -p 9080:9080 -p 9443:9443 --name BibleRestAPI bibledb/BibleRestAPI