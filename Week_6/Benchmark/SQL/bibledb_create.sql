--
-- Create a new database: bibledb
--
CREATE DATABASE IF NOT EXISTS bibledb DEFAULT CHARACTER SET latin1;

--
-- Create a new user on the local host
--
CREATE USER 'bibleuser'@'localhost' IDENTIFIED BY 'biblepassword';

--
-- Grant privileges to bibleuser on bibledb
--
GRANT ALL PRIVILEGES ON bibledb.* TO 'bibleuser'@'localhost';
GRANT USAGE ON *.* TO 'bibleuser'@'localhost';

--
-- Force grants to take affect
--
FLUSH PRIVILEGES;

--
-- Start using the database to add tables
--
USE bibledb;

--
-- Load SQL Scripts from command line
--

-- mysql -u bibleuser -p bibledb < bibledb_bookindex.sql
-- mysql -u bibleuser -p bibledb < bibledb_sectionindex.sql
-- mysql -u bibleuser -p bibledb < bibledb_booksection.sql
-- mysql -u bibleuser -p bibledb < bibledb_kjv.sql

-- Password biblepassword
