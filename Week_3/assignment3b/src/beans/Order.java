
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 3b - JSF Data Grid
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create first application using JSF Technologies
 * 2. Implement MVC design pattern
 * 3. A Java Bean to hold user name data
 * ---------------------------------------------------------------
 */

package beans;

import javax.faces.bean.ViewScoped;
import javax.faces.bean.ManagedBean;

@ViewScoped
@ManagedBean
public class Order {

	String orderNumber = "";
	String productName = "";
	float price = 0.0f;
	int quantity = 0;

	public Order() {
	}

	public Order(String orderNumber, String productName, float price, int quantity) {
		super();
		this.orderNumber = orderNumber;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
	
	
}
