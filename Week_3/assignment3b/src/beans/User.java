
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 3b - JSF Data Grid
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create first application using JSF Technologies
 * 2. Implement MVC design pattern
 * 3. A Java Bean to hold user name data
 * ---------------------------------------------------------------
 */

package beans;

import javax.faces.bean.ViewScoped;
import javax.faces.bean.ManagedBean;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;
// NOTE: MyFaces does not contain validation constraints - cannot locate a Mojarra library that works

@ViewScoped
@ManagedBean
public class User {

	// @NotNull()
	// @Size(min=5, max=15)
	private String firstName;

	// @NotNull()
	// @Size(min=5, max=15)
	private String lastName;

	// Default Constructor
	public User () {
		firstName = "Kelly";
		lastName = "Lamb";
	}

	// Parameterized Constructor
	public User(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	//
	// Define Field assessor methods (Setters/Getters)
	//
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
