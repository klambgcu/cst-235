
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 3b - JSF Data Grid
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create first application using JSF Technologies
 * 2. Implement MVC design pattern
 * 3. A Java Bean to hold user name data
 * ---------------------------------------------------------------
 */

package beans;

import javax.faces.bean.ViewScoped;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

@ViewScoped
@ManagedBean
public class Orders {

	List<Order> orders = new ArrayList<Order>();
	
	public Orders()
	{
		// Default list of ice cream orders
		orders.add(new Order("00001", "Chocolate Ice Cream",     3.00f, 10));
		orders.add(new Order("00002", "Vanilla Ice Cream",       3.00f, 20));
		orders.add(new Order("00003", "Strawberry Ice Cream",    3.00f, 30));
		orders.add(new Order("00004", "Rocky Road Ice Cream",    2.75f, 10));
		orders.add(new Order("00005", "Chocolate Chip Ice Cream",3.25f, 50));
		orders.add(new Order("00006", "Cookie Dough Ice Cream",  2.50f, 10));
		orders.add(new Order("00007", "Spumoni Ice Cream",       2.50f, 20));
		orders.add(new Order("00008", "Neopolitan Ice Cream",    3.25f, 50));
		orders.add(new Order("00009", "Mint Ice Cream",          2.00f, 10));
		orders.add(new Order("00010", "Oreo Crumb Ice Cream",    3.50f, 20));		
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	
	
}
