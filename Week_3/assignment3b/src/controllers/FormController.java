
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-02-22
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 3a - Test Java Server Faces (JSF)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create first application using JSF Technologies
 * 2. Implement MVC design pattern
 * 3. A Controller to handle the form submit information
 * ---------------------------------------------------------------
 */

package controllers;

import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import beans.User;

import javax.faces.bean.ManagedBean;

@ManagedBean
@ViewScoped

public class FormController {

	public String onSubmit(User user) {

		FacesContext context = FacesContext.getCurrentInstance();

		System.out.println("FormController onSubmit() Executing.");
		System.out.println("User Firstname: " + user.getFirstName() + ", LastName: " + user.getLastName());

		context.getExternalContext().getRequestMap().put("user", user);

		return "TestResponse.xhtml";
	}

	public String onFlash(User user) {

		System.out.println("FormController onFlash() Executing.");

		// FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("user", user);

		//return "TestResponse.xhtml?faces-redirect=true";
		return "TestResponse2.xhtml";
	}
}
