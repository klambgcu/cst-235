
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-03-01
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 4a - Test EJBs
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create EJBs
 * 2. Create interfaces and concrete classes to inject/configure
 * 3. Timer Service beans/services
 * ---------------------------------------------------------------
 */

package controllers;

import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import beans.User;
import business.OrdersBusinessInterface;
import business.MyTimerService;

@ManagedBean
@ViewScoped
public class FormController {

    @Inject
    OrdersBusinessInterface service;

    @EJB
    MyTimerService timer;

    public String onSubmit(User user) {

        FacesContext context = FacesContext.getCurrentInstance();

        System.out.println("FormController onSubmit() Executing.");
        System.out.println("User Firstname: " + user.getFirstName() + ", LastName: " + user.getLastName());

        // Execute the injected services method
        service.test();

        timer.setTimer(10000);

        context.getExternalContext().getRequestMap().put("user", user);

        return "TestResponse.xhtml";
    }

    public OrdersBusinessInterface getService() {
        return service;
    }
}
