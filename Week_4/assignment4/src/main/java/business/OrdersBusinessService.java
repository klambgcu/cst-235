
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-03-01
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 4a - Test EJBs
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create EJBs
 * 2. Create interfaces and concrete classes to inject/configure
 * 3. Timer Service beans/services
 * ---------------------------------------------------------------
 */

package business;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import java.util.ArrayList;
import java.util.List;
import beans.Order;

@Stateless
@Local(OrdersBusinessInterface.class)
@Alternative
public class OrdersBusinessService implements OrdersBusinessInterface {

    private List<Order> orders = new ArrayList<Order>();

    public OrdersBusinessService() {
        // Default list of ice cream orders
        orders.add(new Order("00001", "Chocolate Ice Cream",     3.00f, 10));
        orders.add(new Order("00002", "Vanilla Ice Cream",       3.00f, 20));
        orders.add(new Order("00003", "Strawberry Ice Cream",    3.00f, 30));
        orders.add(new Order("00004", "Rocky Road Ice Cream",    2.75f, 10));
        orders.add(new Order("00005", "Chocolate Chip Ice Cream",3.25f, 50));
        orders.add(new Order("00006", "Cookie Dough Ice Cream",  2.50f, 10));
        orders.add(new Order("00007", "Spumoni Ice Cream",       2.50f, 20));
        orders.add(new Order("00008", "Neopolitan Ice Cream",    3.25f, 50));
        orders.add(new Order("00009", "Mint Ice Cream",          2.00f, 10));
        orders.add(new Order("00010", "Oreo Crumb Ice Cream",    3.50f, 20));		
    }

    @Override
    public void test() {
        System.out.println("Hello from the OrdersBusinessService");
    }

    @Override
    public List<Order> getOrders() {
        return orders;
    }

    @Override
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
