@echo off
call mvn clean package
call docker build -t klamb/assignment4a .
call docker rm -f assignment4a
call docker run -d -p 9080:9080 -p 9443:9443 --name assignment4a klamb/assignment4a