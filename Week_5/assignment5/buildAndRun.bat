@echo off
call mvn clean package
call docker build -t klamb/assignment5 .
call docker rm -f assignment5
call docker run -d -p 9080:9080 -p 9443:9443 --name assignment5 klamb/assignment5