#!/bin/sh
mvn clean package && docker build -t klamb/assignment5 .
docker rm -f assignment5 || true && docker run -d -p 9080:9080 -p 9443:9443 --name assignment5 klamb/assignment5