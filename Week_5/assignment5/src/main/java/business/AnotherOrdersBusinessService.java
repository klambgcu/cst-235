
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-03-01
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 4a - Test EJBs
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create EJBs
 * 2. Create interfaces and concrete classes to inject/configure
 * 3. Timer Service beans/services
 * ---------------------------------------------------------------
 */

package business;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import java.util.ArrayList;
import java.util.List;
import beans.Order;

@Stateless
@Local(OrdersBusinessInterface.class)
@Alternative
public class AnotherOrdersBusinessService implements OrdersBusinessInterface {

    private List<Order> orders = new ArrayList<Order>();

    public AnotherOrdersBusinessService() {
        // Default list of ice cream orders
        orders.add(new Order("00001", "Chocolate Candy",     1.00f,  1));
        orders.add(new Order("00002", "Vanilla Candy",       1.00f,  2));
        orders.add(new Order("00003", "Strawberry Candy",    1.00f,  3));
        orders.add(new Order("00004", "Rocky Road Candy",    1.75f,  4));
        orders.add(new Order("00005", "Chocolate Chip Candy",1.25f,  5));
        orders.add(new Order("00006", "Cookie Dough Candy",  1.50f,  6));
        orders.add(new Order("00007", "Spumoni Candy",       1.50f,  7));
        orders.add(new Order("00008", "Neopolitan Candy",    1.25f,  8));
        orders.add(new Order("00009", "Mint Candy",          1.00f,  9));
        orders.add(new Order("00010", "Oreo Crumb Candy",    1.50f, 10));		
    }

    @Override
    public void test() {
        System.out.println("Hello from the AnotherOrdersBusinessService");
    }

    @Override
    public List<Order> getOrders() {
        return orders;
    }

    @Override
    public void setOrders(List<Order> orders) {

    }
}
