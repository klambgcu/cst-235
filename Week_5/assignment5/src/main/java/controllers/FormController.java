
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-03-01
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 4a - Test EJBs
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create EJBs
 * 2. Create interfaces and concrete classes to inject/configure
 * 3. Timer Service beans/services
 * ---------------------------------------------------------------
 */

package controllers;

import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import beans.User;
import business.OrdersBusinessInterface;
import business.MyTimerService;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

@ManagedBean
@ViewScoped
public class FormController {

    @Inject
    OrdersBusinessInterface service;

    @EJB
    MyTimerService timer;

    public String onSubmit(User user) {

        FacesContext context = FacesContext.getCurrentInstance();

        System.out.println("FormController onSubmit() Executing.");
        System.out.println("User Firstname: " + user.getFirstName() + ", LastName: " + user.getLastName());

        // Execute the injected services method
        service.test();

        timer.setTimer(10000);

        context.getExternalContext().getRequestMap().put("user", user);
        
        getAllOrders();
        insertOrder();
        getAllOrders();

        return "TestResponse.xhtml";
    }

    public OrdersBusinessInterface getService() {
        return service;
    }
            
    private void getAllOrders() {
        
        String DBURL = "jdbc:postgresql://localhost:5432/postgres";
        String DBUSER = "postgres";
        String DBPASS = "root";
        String SELECT_ORDERS = "SELECT * FROM testapp.ORDERS";

        Connection conn = null;
        
        try
        {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(SELECT_ORDERS);
            
            while (rs.next()) {
                
                int ID = rs.getInt("ID");
                String PRODUCT_NAME = rs.getString("PRODUCT_NAME");
                float PRICE = rs.getFloat("PRICE");
                
                System.out.println("Order: ID=" + ID + ", Product Name= " + PRODUCT_NAME + ", Price=" + PRICE );
            }
            
            rs.close();
            statement.close();
            
        } catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Failure!!");
        } finally {
            if (conn != null) {
                try{
                conn.close();
                } catch (Exception e) {
                }
            }
        }
    }

    private void insertOrder() {
        
        String DBURL = "jdbc:postgresql://localhost:5432/postgres";
        String DBUSER = "postgres";
        String DBPASS = "root";
        String INSERT_ORDER = "INSERT INTO testapp.ORDERS (ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY) VALUES('001122334455', 'This was inserted new', 25.00, 100)";

        Connection conn = null;
        
        try
        {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(DBURL, DBUSER, DBPASS);
            Statement statement = conn.createStatement();
            statement.executeUpdate(INSERT_ORDER);
            
            statement.close();
            
        } catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Failure!!");
        } finally {
            if (conn != null) {
                try{
                    conn.close();
                } catch (Exception e) {
                }
            }
        }
  
        
        
    }
    

    
}
