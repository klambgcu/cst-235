#!/bin/sh
mvn clean package && docker build -t klamb/assignment7 .
docker rm -f assignment7 || true && docker run -d -p 9080:9080 -p 9443:9443 --name assignment7 klamb/assignment7