package beans;

import javax.faces.bean.ViewScoped;
import javax.faces.bean.ManagedBean;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-03-21
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 6b - Test EJBs
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create EJBs
 * 2. Create interfaces and concrete classes to inject/configure
 * 3. Timer Service beans/services
 * ---------------------------------------------------------------
 */

@ViewScoped
@ManagedBean
@XmlRootElement(name="Order") 
public class Order {

	String orderNumber = "";
	String productName = "";
	float price = 0.0f;
	int quantity = 0;

	public Order() {
	}

	public Order(String orderNumber, String productName, float price, int quantity) {
		super();
		this.orderNumber = orderNumber;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

        @Override
        public String toString() {
            
            return "orderNumber=" + this.orderNumber +
		   " productName=" + this.productName +
		   " price=" + this.price +
		   " quantity=" + this.quantity;
        }



}
