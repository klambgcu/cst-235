
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-03-01
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 7 - Test EJBs
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create EJBs
 * 2. Create interfaces and concrete classes to inject/configure
 * 3. Timer Service beans/services
 * ---------------------------------------------------------------
 */

package beans;

import java.security.Principal;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ViewScoped
@ManagedBean
public class User {

        @PostConstruct
        void init() {
            // Get the logged in Principle
            Principal principle = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
            if (principle == null) {
                setFirstName("Unknown");
                setLastName("");
            } else {
                setFirstName(principle.getName());
                setLastName("");
            }
        }

	@NotNull()
	@Size(min=5, max=15)
	private String firstName;

	@NotNull()
	@Size(min=5, max=15)
	private String lastName;

	// Default Constructor
	public User () {
		firstName = "Kelly";
		lastName = "Lamb";
	}

	// Parameterized Constructor
	public User(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	//
	// Define Field assessor methods (Setters/Getters)
	//
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
