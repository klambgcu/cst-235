package business;

import beans.Order;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-03-21
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 6b - Test Rest Service
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create RESTful services
 * 
 * 
 * ---------------------------------------------------------------
 */

/**
 * REST Web Service
 *
 * @author Kelly Lamb
 */
@RequestScoped
@Path("/orders")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class OrdersRestService {

    @Context
    private UriInfo context;

    @Inject
    OrdersBusinessInterface service;

    /**
     * Creates a new instance of OrdersRestService
     */
    public OrdersRestService() {
    }

    
    @GET
    @Path("/getjson")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Order> getOrdersAsJson() {
        
        return service.getOrders();
    }
    
    @GET
    @Path("/getxml")
    @Produces(MediaType.APPLICATION_XML)
    public Order[] getOrdersAsXml() {
        
        Order[] orders = new Order[service.getOrders().size()];
        orders = service.getOrders().toArray(orders);
        return orders;
    }

    /**
     * Add an order to the order entry database via Post/JSON
     * 
     * @param order
     * @return Response 
     */
    @POST
    @Path("/addorder")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addOrder(Order order) {
        
        // Log message that order was received
        System.out.println("RESTful Service: AddOrder: New Order=" + order.toString());
        
        // Return a successful response for test purposes
        return Response.status(javax.ws.rs.core.Response.Status.OK).build(); //200
        
	// 400 if missing item
	// 401 if invalid order (exists) or invalid part name (not exists)
	// 500 something went wrong, try again…

    }
    
    /**
     * PUT method for updating or creating an instance of OrdersRestService
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
