
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-02-15
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 2b - Test Java Server Faces (JSF)
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create first application using JSF Technologies
 * 2. Implement MVC design pattern
 * 3. A Java Bean to hold user name data
 * ---------------------------------------------------------------
 */

package beans;

import javax.faces.bean.ViewScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean 
@ViewScoped 

public class User {

	private String firstName;
	private String lastName;

	// Default Constructor
	public User () {
		firstName = "Kelly";
		lastName = "Lamb";
	}
	
	// Parameterized Constructor
	public User(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	//
	// Define Field assessor methods (Setters/Getters)
	//
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}	
}
