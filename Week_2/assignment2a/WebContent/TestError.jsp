<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Test Error</title>
</head>
<body>

Validation Error:

<p>
Either the first or last name was not entered.
Both are required fields
</p>

First Name: <%= request.getAttribute("firstname") %> <br>
Last Name: <%=  request.getAttribute("lastname") %> <br>
<br>
Please reenter these fiels in the TestForm.jsp page! 

</body>
</html>