<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Test Response</title>
</head>
<body>

Greetings! <br>
<br>
First Name: <%= request.getAttribute("firstname") %> <br>
Last Name: <%=  request.getAttribute("lastname") %> <br>
<br>
I hope you have a wonderful day!
</body>
</html>