
/**
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-02-15
 * Class     : CST-235 Computer Programming III
 * Professor : Mohamed Mneimneh
 * Assignment: Assignment 2a - Test Servlet JSP
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Create Test Servlet to process names using JSP
 * 2. Implement MVC design pattern
 * ---------------------------------------------------------------
 */

package assignment;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet(description = "Assignment 2a - Test Servlet", urlPatterns = { "/TestServlet" })
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TestServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		System.out.println("TestServlet - Init() Method called.");
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		System.out.println("TestServlet - Destroy() Method called.");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String firstName = request.getParameter("firstname");
		String lastName = request.getParameter("lastname");

		// Return test response names
		// response.getWriter().append("The firstname is " + firstName + ".<br>");
		// response.getWriter().append("The lastname is " + lastName + ".<br>");

		// Forward names to a test response page
		request.setAttribute("firstname", firstName);
		request.setAttribute("lastname", lastName);
		request.getRequestDispatcher("TestResponse.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String firstName = request.getParameter("firstname");
		String lastName = request.getParameter("lastname");

		// Validate entry values - not null, not blank
		if ((null == firstName) || (null == lastName) || ("".equals(firstName)) || ("".equals(lastName))) {
			// Forward names to a error response page
			request.setAttribute("firstname", firstName);
			request.setAttribute("lastname", lastName);
			request.getRequestDispatcher("TestError.jsp").forward(request, response);
		} else {
			// Forward names to a test response page
			request.setAttribute("firstname", firstName);
			request.setAttribute("lastname", lastName);
			request.getRequestDispatcher("TestResponse.jsp").forward(request, response);
		}
	}

}
